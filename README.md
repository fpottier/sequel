# Sequel

Sequences of type `'a Seq.t` are a universal exchange format, which many
modules in OCaml's standard library are able to produce and consume.

Unfortunately, OCaml's
[Seq](https://caml.inria.fr/pub/docs/manual-ocaml/libref/Seq.html) module
offers a limited number of facilities for working with sequences.

The `Sequel` library complements `Seq` by offering a collection of functions
that help construct and consume sequences. Like Haskell's List library, it can
be thought of as a fairly expressive and low-performance stream library.

**Caveat**: at this time, **this library has not been thoroughly tested**. It
may have bugs. Furthermore, its API is not stable and is liable to change in
future releases.
