(******************************************************************************)
(*                                                                            *)
(*                                    Sek                                     *)
(*                                                                            *)
(*          Arthur Charguéraud, Émilie Guermeur and François Pottier          *)
(*                                                                            *)
(*  Copyright Inria. All rights reserved. This file is distributed under the  *)
(*  terms of the GNU Lesser General Public License as published by the Free   *)
(*  Software Foundation, either version 3 of the License, or (at your         *)
(*  option) any later version, as described in the file LICENSE.              *)
(*                                                                            *)
(******************************************************************************)

(* This OCaml script shows how one can play with sequences in the OCaml
   toplevel environment. It can be executed by typing [ocaml play.ml].
   Do not forget to install the library first, e.g., [make reinstall]. *)

#use "topfind";;
#require "sequel";;
open Sequel;;

(* Exercise: computing the fringe of a tree. *)

type 'a tree =
  | Leaf
  | Node of 'a tree * 'a * 'a tree

(* In this formulation, we think in terms of sequences, and use a continuation
   [k] (a sequence) in order to avoid the use of sequence concatenation. *)

let rec fringe (t : 'a tree) (k : 'a seq) =
  match t with
  | Leaf ->
      k
  | Node (u, x, v) ->
      fringe u (cons x (fringe v k))

(* In this alternate formulation, we think in terms of producers, which allows
   us to never mention the continuation, although it is there, implicitly. *)

(* Both formulations have linear time complexity. The second formulation is
   more expensive than the first one by a constant factor, because it involves
   constructing a tree of closures in memory. *)

(* The use of the [delay] combinator is optional, but recommended. Without it,
   the entire tree [t] is traversed as soon as [fringe t] is evaluated, before
   the sequence [fringe t] is forced. With it, the tree [t] is traversed only
   on demand, as the elements of the sequence [fringe t] are demanded. *)

open P

let rec fringe (t : 'a tree) : 'a producer =
  delay (fun () ->
    match t with
    | Leaf ->
        empty
    | Node (u, x, v) ->
        fringe u ++ return x ++ fringe v
  )

(* An example use of [fringe]. *)

let t =
  Node (
    Node (Leaf, 1, Leaf),
    2,
    Node (Leaf, 3, Leaf)
  )

let xs : int list =
  t |> fringe |> run |> to_list
