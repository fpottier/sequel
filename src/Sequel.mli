(** A sequence of type ['a Seq.t] represents a finite or infinite sequence of
   elements. These elements are made available when a consumer demands them.
   The type ['a Seq.t] is defined as a synonym for [unit -> 'a Seq.node]. It
   is a function type: therefore, it is opaque. This type does not reveal
   whether the elements are computed ahead of time or computed when they are
   demanded. It does not reveal whether the sequence is ephemeral (which means
   that it can be used at most once) or persistent (which means that it can be
   used several time). In the case of an ephemeral sequence, it does not
   reveal what side effects might be caused by demanding the elements. In the
   case of a persistent sequence, it does not reveal whether the elements are
   re-computed every time they are demanded or memoized. *)
type 'a seq =
  'a Seq.t

(* -------------------------------------------------------------------------- *)

(** {1 Destructors} *)

(** When we write that a sequence [xs] is {i consumed} by an operation, this
    means that, if the sequence is ephemeral, then it cannot be used again. A
    persistent sequence can be used as many times as desired. *)

(** When we write that a sequence [xs] is forced down to depth [k], this means
    that the first [k] elements of the sequence are demanded. *)

(** The exception [ForcedTwice] is raised when an element produced by [once
    xs] is demanded twice. *)
exception ForcedTwice

(* -------------------------------------------------------------------------- *)

(** {2 Depth-One Destructors} *)

(** [force xs] forces the sequence [xs] down to depth 1. It is synonymous with
    [xs()], but is more explicit. The sequence [xs] is consumed, so (if it is
    ephemeral) it can no longer be used. Instead, the result of [force xs], a
    node, can be used. *)
val force:     'a seq -> 'a Seq.node

(** [is_empty xs] forces [xs] down to depth 1 and returns a Boolean result
    that indicates whether [xs] is empty. [xs] is consumed. Because of this,
    this function is usually applied only to persistent sequences. *)
val is_empty:  'a seq -> bool

(** [head_tail xs] forces [xs] down to depth 1 and returns a pair of its first
    element and its tail. [xs] must be nonempty. [xs] is consumed. If [xs] is
    persistent, then [head_tail xs] is the same as [(head xs, tail xs)]. If
    [xs] is ephemeral, then evaluating [head_tail xs] is permitted, whereas
    evaluating [(head xs, tail xs)] is not. *)
val head_tail: 'a seq -> 'a * 'a seq

(** [head xs] forces [xs] down to depth 1 and returns its first element.
    [xs] must be nonempty. [xs] is consumed. *)
val head:      'a seq -> 'a

(** [head_opt xs] forces [xs] down to depth 1 and returns its first element,
    if [xs] is nonempty. Otherwise, it returns [None]. [xs] is consumed. *)
val head_opt:  'a seq -> 'a option

(** [tail xs] forces [xs] down to depth 1 and returns its tail. [xs] must be
    nonempty. [xs] is consumed. *)
val tail:      'a seq -> 'a seq

(** [tail_opt xs] forces [xs] down to depth 1 and returns its tail, if [xs] is
    nonempty. Otherwise, it returns [None]. [xs] is consumed. *)
val tail_opt:  'a seq -> 'a seq option

(* -------------------------------------------------------------------------- *)

(** {2 Known-Depth Destructors} *)

(** [get k xs] forces [xs] down to depth [k] and returns its [k]-th element.
    Elements are indexed from 0 and up. [xs] must have at least [k+1] elements.
    [xs] is consumed. Because of this, this function is usually applied only to
    persistent sequences. *)
val get:     int -> 'a seq -> 'a

(** If the sequence [xs] has at least [k+1] elements, then [get_opt k xs]
    forces [xs] down to depth [k] and returns its [k]-th element. Elements are
    indexed from 0 and up. Otherwise, it returns [None]. [xs] is consumed.
    Because of this, this function is usually applied only to persistent
    sequences. *)
val get_opt: int -> 'a seq -> 'a option

(** [drop k xs] returns the sequence [xs] deprived of its first [k] elements.
    [xs] is consumed. No side effect takes place when [drop k xs] is invoked;
    however, as soon as the sequence [drop k xs] is forced down to depth 1,
    the sequence [xs] is forced down to depth [k+1]. The sequence [drop k xs]
    is a suffix of [xs] and is of the same (ephemeral/persistent) nature as
    [xs]. *)
val drop: int -> 'a seq -> 'a seq

(** [split k xs] returns a pair of the first [k] elements of the sequence [xs]
    and the sequence [xs] deprived of its first [k] elements. The sequence [xs]
    is immediately forced down to depth [k]. The first component of the pair is
    a persistent sequence, whereas the second component of the pair is a suffix
    of [xs] and is of the same (ephemeral/persistent) nature as [xs]. *)
val split: int -> 'a seq -> 'a seq * 'a seq

(** [to_subarray xs a i j] writes the first [j - i] elements of the sequence
    [xs] to the array [a], from the start index [i] (included) to the end index
    [j] (excluded). It returns a pair of the first index that was not written
    and the remainder of the sequence. The sequence [xs] is forced down to
    depth [j - i]. If [j] is less than [i], nothing is written and the pair
    [(i, xs)] is returned. *)
val to_subarray: 'a seq -> 'a array -> int -> int -> int * 'a seq

(** [to_array xs a] writes the first [n] elements of the sequence [xs] to the
    array [a], where [n] is the length of the array [a]. It returns a pair of
    the first index that was not written and the remainder of the sequence.
    The sequence [xs] is forced down to depth [n]. *)
val to_array:    'a seq -> 'a array               -> int * 'a seq

(** [to_subbytes xs a i j] writes the first [j - i] elements of the sequence
    [xs] to the byte buffer [b], from the start index [i] (included) to the end
    index [j] (excluded). It returns a pair of the first index that was not
    written and the remainder of the sequence. The sequence [xs] is forced down
    to depth [j - i]. If [j] is less than [i], nothing is written and the pair
    [(i, xs)] is returned. *)
val to_subbytes: char seq -> bytes -> int -> int -> int * char seq

(** [to_bytes xs b] writes the first [n] elements of the sequence [xs] to the
    byte buffer [b], where [n] is the length of the buffer [b]. It returns a
    pair of the first index that was not written and the remainder of the
    sequence. The sequence [xs] is forced down to depth [n]. *)
val to_bytes:    char seq -> bytes               -> int * char seq

(* -------------------------------------------------------------------------- *)

(** {2 Unknown-Depth Destructors} *)

(** The following functions force their argument(s) down to an a priori
    unknown depth, and consume their argument(s). *)

(** [exists f xs] is [true] if and only if some element of [xs] satisfies [f].
    [xs] must be finite. Because it consumes its argument, this function is
    usually applied only to persistent sequences. *)
val exists: ('a -> bool) -> 'a seq -> bool

(** [for_all f xs] is [true] if and only if all elements of [xs] satisfy [f].
    [xs] must be finite. Because it consumes its argument, this function is
    usually applied only to persistent sequences. *)
val for_all: ('a -> bool) -> 'a seq -> bool

(** [equal eq xs ys] returns [true] iff the sequences [xs] and [ys] are
    pointwise equal. The user-supplied function [eq] is used to test elements
    for equality. At least one of the sequences [xs] and [ys] must be finite.
    Because it consumes its arguments, this function is usually applied only to
    persistent sequences. *)
val equal: ('a -> 'b -> bool) -> 'a seq -> 'b seq -> bool

(** If [cmp] implements a preorder on elements, then [compare cmp] implements
    the lexicographic preorder on sequences. (A preorder is an antisymmetric
    and transitive relation. For more details on comparison functions in OCaml,
    see the documentation of [Array.sort].) At least one of the sequences that
    are being compared must be finite. Because it consumes its arguments,
    [compare] is usually applied only to persistent sequences. *)
val compare : ('a -> 'b -> int) -> 'a seq -> 'b seq -> int

(** If at least one element of the sequence [xs] satisfies [f], then [find f
    xs] returns [Some (x, xs')], where [x] is the first element of [xs] that
    satisfies [f] and [xs'] is the remainder of the sequence. If no element of
    [xs] satisfies [f] and if [xs] is finite then [find f xs] returns [None]. *)
val find: ('a -> bool) -> 'a seq -> ('a * 'a seq) option

(** If at least one element of the sequence [xs] satisfies [f], then [from f
    xs] returns [cons x xs'], where [x] is the first element of [xs] that
    satisfies [f] and [xs'] is the remainder of the sequence. If no element of
    [xs] satisfies [f] and if [xs] is finite then [from f xs] returns [nil].
    In other words, [from f xs] returns a suffix of [xs] that begins with
    the first element of [xs] that satisfies [f]. *)
val from: ('a -> bool) -> 'a seq -> 'a seq

(** [to_iterator xs] returns an iterator on the elements of the sequence [xs].
    An iterator is a function that returns a new element every time it is
    invoked, and returns [None] when there are no more elements. An iterator
    has mutable internal state and can be used only once. *)
val to_iterator: 'a seq -> (unit -> 'a option)

(* -------------------------------------------------------------------------- *)

(** {2 Complete Destructors} *)

(** The following functions force their argument, a sequence [xs], as far
    down as possible. Each of these functions requires [xs] to be finite,
    and consumes [xs]. *)

(** [length xs] returns the length of the sequence [xs]. Because it consumes
    [xs], it is usually applied only to persistent sequences. *)
val length: 'a seq -> int

(** [iter f xs] applies the function [f] in turn to every element of the
    sequence [xs]. *)
val iter: ('a -> unit) -> 'a seq -> unit

(** [fold_left f s xs] applies the function [f] in turn to every element of
    the sequence [xs], while threading an accumulator, whose initial value is
    [s]. *)
val fold_left: ('accu -> 'a -> 'accu) -> 'accu -> 'a seq -> 'accu

(** The exception [LengthMismatch (c, m, n)] is raised by [iter2]. The
    comparison code [c] indicates which of the lengths [m] and [n] is smaller:
    [-1] means [m] is smaller, whereas [+1] means [n] is smaller. Among the two
    suspensions [m] and [n], the one that represents the greater length is
    unevaluated; forcing it can have side effects if the sequences [xs] and
    [ys] that have been passed to [iter2] are impure. *)
exception LengthMismatch of int * int Lazy.t * int Lazy.t

(** [iter2 f xs ys] repeatedly draws an element [x] from the sequence [xs], an
    element [y] from the sequence [ys], and invokes [f x y]. If the sequences
    [xs] and [ys] have the same length, it terminates normally. If [xs] is
    shorter than [ys], then an exception of the form [LengthMismatch (-1, m,
    n)] is raised, where [m] represents the length of [xs], [n] represents the
    length of [ys], and forcing [n] causes [ys] to be forced completely.
    Symmetrically, if [ys] is shorter than [xs], then an exception of the form
    [LengthMismatch (+1, m, n)] is raised, where [m] represents the length of
    [xs], [n] represents the length of [ys], and forcing [m] causes [xs] to
    be forced completely. *)
val iter2: ('a -> 'b -> unit) -> 'a seq -> 'b seq -> unit

(** [to_list xs] is the list of the elements of the sequence [xs]. *)
val to_list: 'a seq -> 'a list

(** [rev_to_list xs] is the list of the elements of the sequence [xs],
    in reverse order. It is equivalent to [List.rev (to_list xs)]. It
    is more efficient than [to_list]. *)
val rev_to_list: 'a seq -> 'a list

(** [last xs] returns the last element of the sequence [xs]. The sequence
    [xs] must be nonempty. *)
val last: 'a seq -> 'a

(** If the sequence [xs] is nonempty, then [last_opt xs] returns its last
    element. Otherwise, it returns [None]. *)
val last_opt: 'a seq -> 'a option

(** If the sequence [xs] is nonempty, then [last1 x xs] returns its last
    element. Otherwise, it returns [x]. *)
val last1: 'a -> 'a seq -> 'a

(** [to_channel c xs] writes the elements of the sequence [xs] to the output
    channel [c]. It returns the number of characters written, that is, the
    length of [xs]. *)
val to_channel: out_channel -> char seq -> int

(* -------------------------------------------------------------------------- *)

(* {1 Basic Constructors} *)

(** [nil] is the empty sequence. *)
val nil: 'a seq

(** [cons x xs] is a sequence that begins with the element [x] and continues
    with the sequence [xs]. Note that [cons] is a function, hence is strict: in
    the expression [cons x xs], both [x] and [xs] are computed before the
    function [cons] is called. *)
val cons: 'a -> 'a seq -> 'a seq

(** [singleton x] is the one-element sequence that contains the element [x]. *)
val singleton: 'a -> 'a seq

(** [(>>=)] is the sequencing operator of the sequence monad. This monad is
    known as the list monad in Haskell, and can be used to represent
    nondeterministic computations. The definition of this function involves
    concatenation, which is inefficient; its use is discouraged. *)
val (>>=): 'a seq -> ('a -> 'b seq) -> 'b seq

(* -------------------------------------------------------------------------- *)

(** {1 Producers} *)

(** The sequence [append xs ys] is the concatenation of the sequences [xs] and
    [ys]. The use of [append] should be avoided, as its complexity is linear
    in the length of [xs]. *)
val append: 'a seq -> 'a seq -> 'a seq
val (++)  : 'a seq -> 'a seq -> 'a seq

(** The submodule [P] offers a collection of sequence combinators. *)
module P : sig

(** [unfold] is the primitive corecursor, which means that it can in theory
    be used to build every sequence. [unfold f s] produces the elements
    obtained by successively applying [f] to [s]. *)
val unfold: ('s -> ('a * 's) option) -> 's -> 'a seq

(** [take n xs] produces the first [n] elements of [xs]. If [xs] has fewer
    than [n] elements, then [take n xs] is equivalent to [xs]. *)
val take: int -> 'a seq -> 'a seq

(** [replicate n x] produces [n] copies of [x]. *)
val replicate: int -> 'a -> 'a seq

(** [up i j] produces the integers between [i] included and [j] excluded,
    counting up. *)
val up: int -> int -> int seq

(** [down i j] produces the integers between [i] excluded and [j] included,
    counting down. *)
val down: int -> int -> int seq

(** [option ox] produces nothing if [ox] is [None] and produces the element
    [x] if [ox] is [Some x]. It is equivalent to [Option.to_seq ox]. *)
val option: 'a option -> 'a seq

(** [list xs] produces the elements of the list [xs].
    It is equivalent to [List.to_seq xs]. *)
val list: 'a list -> 'a seq

(** [subarray a i j] produces the elements found in the array [a] between the
    start index [i] (included) and the end index [j] (excluded). *)
val subarray: 'a array -> int -> int -> 'a seq

(** [array a] produces the elements of the array [a].
    It is equivalent to [Array.to_seq a]. *)
val array: 'a array -> 'a seq

(** [substring s i j] produces the characters found in the string [s]
    between the start index [i] (included) and the end index [j]
    (excluded). *)
val substring: string -> int -> int -> char seq

(** [string s] produces the characters in the string [s].
    It is equivalent to [String.to_seq s]. *)
val string: string -> char seq

(** [iterator it] transmits the elements produced by the iterator [it]. An
    iterator is a function that returns a new element every time it is invoked,
    and returns [None] if there are no more elements. [iterator] can be viewed
    as a simplified version of [unfold] where the state has type [unit]. Its
    result is an ephemeral sequence: it cannot be consumed twice. *)
val iterator: (unit -> 'a option) -> 'a seq

(** [channel c] produces the characters obtained by reading the channel [c].
    If and when the end of the channel is reached, the channel is closed.
    It is an ephemeral sequence: it cannot be consumed twice. *)
val channel: in_channel -> char seq

(** If [f] maps one element [x] to one element, then [map f xs] produces
    the sequence of the elements [f x], where [x] ranges over [xs]. *)
val map: ('a -> 'b) -> 'a seq -> 'b seq

(** If [f] maps a state and an element to a new state, then [scanl f s xs]
    produces the sequence of states obtained by iterating [f], beginning
    in state [s], over the sequence [xs]. *)
val scanl: ('s -> 'a -> 's) -> 's -> 'a seq -> 's seq

(** [init n f] produces the sequence [f 0], [f 1], ..., [f (n-1)]. *)
val init: int -> (int -> 'a) -> 'a seq

(** If [xss] is a sequence of sequences, then [flatten xss] produces the
    concatenation of the sequences [xs], where [xs] ranges over [xss]. *)
val flatten: 'a seq seq -> 'a seq

(** [filter f xs] produces the subsequence of the elements of [xs]
    that satisfy [f]. *)
val filter: ('a -> bool) -> 'a seq -> 'a seq

(** [while_ f xs] produces the longest prefix of the sequence [xs] whose
    elements satisfy [f]. *)
val while_: ('a -> bool) -> 'a seq -> 'a seq

(** [until f xs] produces the longest prefix of the sequence [xs] whose
    elements do not satisfy [f]. *)
val until: ('a -> bool) -> 'a seq -> 'a seq

(** [on bs xs] produces the subsequence of the elements of [xs]
    such that the matching bit in the sequence [bs] is [true].
    Its length is the minimum of the lengths of [bs] and [xs]. *)
val on: bool seq -> 'a seq -> 'a seq

(** [rev xs] produces the elements of the sequence [xs] in reverse order. No
    side effect takes place when [rev xs] is invoked. When its elements are
    demanded, the whole sequence [xs] is forced at once, and a data structure
    of linear size is allocated in the heap. *)
val rev: 'a seq -> 'a seq

(** [memoize xs] produces the elements of the sequence [xs]. Regardless of
    whether [xs] is ephemeral or persistent, the sequence produced
    by [memoize xs] is persistent: it can be demanded several times. *)
val memoize: 'a seq -> 'a seq

(** [once xs] produces the elements of the sequence [xs]. Regardless of
    whether [xs] is ephemeral or persistent, the sequence produced by
    [once xs] is ephemeral: it must be demanded at most once. If an element of
    this segment is demanded more than once, then the exception [ForcedTwice]
    is raised. This can be useful for debugging purposes. Once the code is
    believed to be correct, [once xs] can be replaced with [emit xs]. *)
val once: 'a seq -> 'a seq

(** [precompute xs] produces the elements of the sequence [xs]. Regardless of
    whether [xs] is ephemeral or persistent, the sequence produced by
    [precompute xs] is persistent: it can be demanded several times. In that
    respect, [precompute] is analogous to [memoize]. However, unlike [memoize],
    where [xs] is evaluated on demand, [precompute] forces [xs] completely and
    immediately, and stores the elements in memory. [precompute xs] is in fact
    a short-hand for [list (to_list xs)]. *)
val precompute: 'a seq -> 'a seq

(** The function call [precompute xs] forces the sequence [xs] immediately. If
    one wishes to force [xs] only when the first element of [precompute xs] is
    demanded, one should write [fun () -> precompute xs ()]. *)

(** [zip xs ys] produces a sequence of elements of the form [(x, y)], where
    [x] is drawn from [xs] and [y] is (synchronously) drawn from [ys].
    Its length is the minimum of the lengths of [xs] and [ys]. *)
val zip: 'a seq -> 'b seq -> ('a * 'b) seq

(** [map2 f xs ys] produces a sequence of elements of the form [f x y],
    where [x] is drawn from [xs] and [y] is (synchronously) drawn from [ys].
    Its length is the minimum of the lengths of [xs] and [ys]. *)
val map2: ('a -> 'b -> 'c) -> 'a seq -> 'b seq -> 'c seq

(** [pack k bs] is a sequence of [k]-bit integers. The bits in the input
    sequence [bs] are divided into groups of [k] consecutive bits, and each
    group is packed into an integer value. The first bit becomes the most
    significant bit. If the number of elements in the sequence [bs] is not a
    multiple of [k], an appropriate number of zero bits are inserted at the end
    as padding. *)
val pack: int -> bool seq -> int seq

(** [unpack k xs] is a sequence of bits. Each integer value in the sequence
    [xs] is regarded as a sequence of [k] bits, where the most significant bit
    comes first. These sequences are concatenated to form the result. *)
val unpack: int -> int seq -> bool seq

end (* P *)

(* -------------------------------------------------------------------------- *)

(** {1 Infinite Sequences} *)

(* The title should be "Constructors of Infinite Sequences", but that is too
   long to fit in the sideline in the HTML output. *)

(** [repeat x] is an infinite sequence of [x]'s. *)
val repeat: 'a -> 'a seq

(** [ints i] is the infinite sequence of the integers, beginning at [i] and
    counting up. *)
val ints: int -> int seq

(** [iterate f x] is the infinite sequence whose elements are [x], [f x],
    [f (f x)], and so on. *)
val iterate : ('a -> 'a) -> 'a -> 'a seq

(** [iterate1 f x] is the infinite sequence whose
    elements are [f x], [f (f x)], and so on. *)
val iterate1: ('a -> 'a) -> 'a -> 'a seq

(** [unfold_infinitely f s] produces the infinite sequence of
    elements obtained by successively applying [f] to [s]. *)
val unfold_infinitely: ('s -> 'a * 's) -> 's -> 'a seq

(** [cycle] turns a finite producer [xs] into an infinite sequence, which is
    the infinite repetition of [xs]. It [xs] produces an infinite sequence,
    then [cycle xs] is equivalent to [run xs]. [xs] must be persistent. *)
val cycle: ('a seq -> 'a seq) -> 'a seq

(** The fixed point combinator [fix] accepts a recursive definition of a
    sequence, represented by a function [f] of type ['a seq -> 'a seq], and
    constructs its fixed point.

    The function [f] should produce a persistent sequence, otherwise the code
    is likely to be incorrect. In fact, [f] should produce a memoized sequence,
    otherwise the code is likely to have incorrect complexity.

    Although the function [f] seems to have the type of a producer, it is not
    necessarily a producer: it does not necessarily just concatenate elements
    in front of its argument. It can do more complicated things, like
    interleave elements in its argument, etc. The function [f] should be
    productive: that is, in order to produce a prefix of [f xs] of length [k],
    it should be sufficient to demand a prefix of [xs] of length strictly less
    than [k]. In semi-formal terms, one might say that [f] should have type
    [later ('a seq) -> 'a seq]. *)
val fix: ('a seq -> 'a seq) -> 'a seq

(* -------------------------------------------------------------------------- *)

(** {1 Joining} *)

(** The following functions have two input sequences and one output sequence. *)

(** If the sequences [xs] and [ys] are sorted as per the total order [cmp],
    then [merge cmp xs ys] is the sorted sequence obtained by merging them. *)
val merge: ('a -> 'a -> int) -> 'a seq -> 'a seq -> 'a seq

(** The sequence [interleave xs ys] begins with the first element of [xs],
    continues with the first element of [ys], and so on. If one of [xs] and
    [ys] is exhausted, it continues with the rest of the other sequence. *)
val interleave: 'a seq -> 'a seq -> 'a seq

(* -------------------------------------------------------------------------- *)

(** {1 Forking} *)

(** The following functions have one input sequence and two output sequences. *)

(** [unzip] transforms a sequence of pairs into a pair of sequences. [unzip xys]
    is equivalent to [(map fst xys, map snd xys)]. Therefore, [unzip] forces its
    argument [xys] twice. For this reason, [xys] must be persistent. Ideally,
    [xys] should be memoized, or cheap to recompute. If [xys] is ephemeral, or
    costly to recompute, one should memoize it first. *)
val unzip: ('a * 'b) seq -> 'a seq * 'b seq

(** This generic sum type is unfortunately missing (at the time of writing)
    from OCaml's standard library. *)
type ('a, 'b) either =
| Left of 'a
| Right of 'b

(** [dispatch] transforms a sequence of apples and oranges into a pair of a
    sequence of apples and a sequence of oranges. It forces its argument [xys]
    twice. Thus, [xys] must be persistent. Ideally, [xys] should be memoized,
    or cheap to recompute. If [xys] is ephemeral, or costly to recompute, one
    should memoize it first. *)
val dispatch: ('a, 'b) either seq -> 'a seq * 'b seq

(** [partition f xs] returns a pair of the sequence of elements of [xs] that
    satisfy [f] and the sequence of elements of [xs] that do not satisfy [f].
    Memoization takes place internally. This ensures that [xs] is forced at
    most once and the function [f] is applied at moce once to each element of
    [xs]. *)
val partition: ('a -> bool) -> 'a seq -> 'a seq * 'a seq

(* -------------------------------------------------------------------------- *)

(** {1 Sorting} *)

(** If [eq] implements an equality test, then [uniq eq xs] is the sequence
    [xs], deprived of adjacent duplicate elements. As a corollary, if the
    sequence [xs] is sorted according to an ordering that is compatible with
    [eq], then [uniq eq xs] is the sequence [xs], deprived of all duplicate
    elements. Duplicate elements are detected and eliminated on the fly, and
    no mutable state is involved. As a result, the sequence [uniq eq xs] is
    persistent if [xs] is persistent; it is ephemeral if [xs] is ephemeral. *)
val uniq: ('a -> 'a -> bool) -> 'a seq -> 'a seq

(** If [cmp] implements a preorder (as described in the documentation of
    [Array.sort]), and if the sequence [xs] is finite, then [merge cmp xs] is a
    sorted version of the sequence [xs]. A lazy version of merge sort is used.
    As a result, demanding just the first element of [merge cmp xs] requires
    only linear time, whereas forcing [merge cmp xs] all the way down requires
    time [O(n log n)]. The sequence [xs] is consumed only once. The sequence
    [merge cmp xs] itself must be consumed at most once, or must be memoized,
    otherwise some of the work of sorting will be duplicated. *)
val sort: ('a -> 'a -> int) -> 'a seq -> 'a seq
