open Seq

type 'a suspension =
  'a Suspension.suspension

type 'a seq =
  'a t

(* -------------------------------------------------------------------------- *)

(* Basic Constructors. *)

let nil =
  empty
    (* fun () -> Nil *)

(* [cons] exists in [Stdlib.Seq] as of OCaml 4.11. *)

let cons =
  fun x xs () -> Cons (x, xs)

let[@inline] singleton x =
  cons x nil

(* -------------------------------------------------------------------------- *)

(* Depth-One Destructors. *)

let[@inline] force xs =
  xs()

let is_empty xs =
  match force xs with
  | Nil ->
      true
  | Cons (_, _) ->
      false

let head xs =
  match force xs with
  | Nil ->
      invalid_arg "head: empty sequence"
  | Cons (x, _) ->
      x

let head_opt xs =
  match force xs with
  | Nil ->
      None
  | Cons (x, _) ->
      Some x

let tail xs =
  match force xs with
  | Nil ->
      invalid_arg "tail: empty sequence"
  | Cons (_, xs) ->
      xs

let tail_opt xs =
  match force xs with
  | Nil ->
      None
  | Cons (_, xs) ->
      Some xs

let head_tail xs =
  match force xs with
  | Nil ->
      invalid_arg "head_tail: empty sequence"
  | Cons (x, xs) ->
      x, xs

(* -------------------------------------------------------------------------- *)

(* Known-Depth destructors. *)

let rec force_drop (n : int) (xs : 'a seq) : 'a node =
  assert (n > 0);
  match force xs with
  | Nil ->
      Nil
  | Cons (_, xs) ->
      let n = n - 1 in
      if n = 0 then
        force xs
      else
        force_drop n xs

let drop (n : int) (xs : 'a seq) : 'a seq =
  if n = 0 then
    xs
  else
    fun () ->
      force_drop n xs

let get n xs =
  head (drop n xs)

let get_opt n xs =
  head_opt (drop n xs)

let rec split_aux (k : int) (xs : 'a list) (ys : 'a seq) : 'a list * 'a seq =
  (* [xs] is an accumulator. *)
  if k = 0 then
    xs, ys
  else
    match force ys with
    | Nil ->
        xs, nil
    | Cons (y, ys) ->
        split_aux (k - 1) (y :: xs) ys

let split (k : int) (ys : 'a seq) : 'a seq * 'a seq =
  if k = 0 then
    nil, ys
  else
    let xs, ys = split_aux k [] ys in
    List.to_seq (List.rev xs), ys

let rec write (set : 'array -> int -> 'a -> unit) (xs : 'a seq) (a : 'array) i j =
  if i < j then
    match force xs with
    | Nil ->
        i, nil
    | Cons (x, xs) ->
        set a i x;
        write set xs a (i + 1) j
  else
    i, xs

let[@inline] unchecked_to_subarray xs a i j =
  write Array.set xs a i j

let to_subarray xs a i j =
  let n = Array.length a in
  if not (0 <= i && i <= n) then
    invalid_arg "to_subarray: invalid start index";
  if not (i + j <= n) then
    invalid_arg "to_subarray: invalid end index";
  unchecked_to_subarray xs a i j

let to_array xs a =
  let n = Array.length a in
  unchecked_to_subarray xs a 0 n

let[@inline] unchecked_to_subbytes xs b i j =
  write Bytes.set xs b i j

let to_subbytes xs b i j =
  let n = Bytes.length b in
  if not (0 <= i && i <= n) then
    invalid_arg "to_subbytes: invalid start index";
  if not (i + j <= n) then
    invalid_arg "to_subbytes: invalid end index";
  unchecked_to_subbytes xs b i j

let to_bytes xs b =
  let n = Bytes.length b in
  unchecked_to_subbytes xs b 0 n

(* -------------------------------------------------------------------------- *)

(* Unknown-Depth Destructors. *)

let rec exists (f : 'a -> bool) (xs : 'a seq) : bool =
  match force xs with
  | Nil ->
      false
  | Cons (x, xs) ->
      f x || exists f xs

let rec for_all (f : 'a -> bool) (xs : 'a seq) : bool =
  match force xs with
  | Nil ->
      true
  | Cons (x, xs) ->
      f x && for_all f xs

let rec equal eq xs ys =
  match force xs, force ys with
  | Nil, Nil ->
      true
  | Cons (x, xs), Cons (y, ys) ->
      eq x y && equal eq xs ys
  | Nil, Cons (_, _)
  | Cons (_, _), Nil ->
      false

let rec compare cmp xs ys =
  match force xs, force ys with
  | Nil, Nil ->
      0
  | Cons (x, xs), Cons (y, ys) ->
      let c = cmp x y in
      if c <> 0 then c else compare cmp xs ys
  | Nil, Cons (_, _) ->
      -1
  | Cons (_, _), Nil ->
      +1

let rec find f xs =
  match force xs with
  | Nil ->
      None
  | Cons (x, xs) ->
      if f x then
        Some (x, xs)
      else
        find f xs

let rec from f xs =
  match force xs with
  | Nil ->
      nil
  | Cons (x, xs) ->
      if f x then
        cons x xs
      else
        from f xs

(* -------------------------------------------------------------------------- *)

(* Complete Destructors. *)

(* [length] can be defined as an instance of [fold_left], but we prefer to
   manually inline this application. *)

let rec length_aux accu xs =
  match force xs with
  | Nil ->
      accu
  | Cons (_, xs) ->
      length_aux (accu + 1) xs

let[@inline] length xs =
  length_aux 0 xs

let rec iter f xs =
  match force xs with
  | Nil ->
      ()
  | Cons (x, xs) ->
      f x;
      iter f xs

let rec fold_left f accu xs =
  match force xs with
  | Nil ->
      accu
  | Cons (x, xs) ->
      let accu = f accu x in
      fold_left f accu xs

exception LengthMismatch of int * int Lazy.t * int Lazy.t

let rec iter2 f n xs ys =
  (* TODO stop at shortest sequence. Avoid double force. *)
  match force xs, force ys with
  | Nil, Nil ->
      ()
  | Cons (x, xs), Cons (y, ys) ->
      f x y;
      iter2 f (n+1) xs ys
  | Nil, Cons (_, ys) ->
      (* The left-hand sequence is shorter. *)
      let c = -1
      and n1 = lazy n
      and n2 = lazy (n + 1 + length ys) in
      raise (LengthMismatch (c, n1, n2))
  | Cons (_, xs), Nil ->
      (* The right-hand sequence is shorter. *)
      let c = +1
      and n1 = lazy (n + 1 + length xs)
      and n2 = lazy n in
      raise (LengthMismatch (c, n1, n2))

let[@inline] iter2 f xs ys =
  iter2 f 0 xs ys

let[@inline] rev_to_list xs =
  fold_left (fun ys x -> x :: ys) [] xs

let to_list xs =
  List.rev (rev_to_list xs)

let[@inline] last1 x xs =
  fold_left (fun _ x -> x) x xs

let last_opt (xs : 'a seq) : 'a option =
  match force xs with
  | Nil ->
      None
  | Cons (x, xs) ->
      Some (last1 x xs)

let last (xs : 'a seq) : 'a =
  match force xs with
  | Nil ->
      invalid_arg "last: empty sequence"
  | Cons (x, xs) ->
      last1 x xs

let to_channel channel cs =
  fold_left (fun n c ->
    output_char channel c;
    n + 1
  ) 0 cs

let to_iterator xs : unit -> 'a option =
  let s = ref xs in
  fun () ->
    match force !s with
    | Nil ->
        None
    | Cons (x, xs) ->
        s := xs;
        Some x

(* -------------------------------------------------------------------------- *)

(* Producers. *)

(* [append] exists in [Stdlib.Seq] as of OCaml 4.11. *)

let rec raw_append xs ys =
  fun () ->
    match force xs with
    | Nil ->
        force ys
    | Cons (x, xs) ->
        Cons (x, raw_append xs ys)

(* Testing whether the second argument of [append] is [nil] seems cheap.
   This optimization can save significant time and memory when it is
   applicable. *)

let append xs ys =
  if ys == nil then
    xs
  else
    raw_append xs ys

let (++) =
  append

(* -------------------------------------------------------------------------- *)

(* A submodule [P] for producers. *)

exception ForcedTwice =
  Suspension.ForcedTwice

module P = struct

(* [unfold] exists in [Stdlib.Seq] as of OCaml 4.11. *)

let rec unfold f s =
  fun () ->
    match f s with
    | None ->
        Nil
    | Some (x, s) ->
        Cons (x, unfold f s)

let rec take n xs =
  if n = 0 then
    nil
  else
    fun () ->
      match force xs with
      | Nil ->
          Nil
      | Cons (x, xs) ->
          Cons (x, take (n - 1) xs)

let rec replicate n x =
  if n = 0 then
    nil
  else
    fun () ->
      Cons (x, replicate (n - 1) x)

let rec up i j =
  fun () ->
    if i < j then begin
      Cons (i, up (i + 1) j)
    end
    else
      Nil

let rec down i j =
  fun () ->
    if i > j then begin
      Cons (i - 1, down (i - 1) j)
    end
    else
      Nil

let option ox =
  match ox with
  | None ->
      nil
  | Some x ->
      singleton x

let rec list xs =
  fun () ->
    match xs with
    | [] ->
        Nil
    | x :: xs ->
        Cons (x, list xs)

let rec unchecked_read get a i j =
  fun () ->
    if i < j then
      Cons (get a i, unchecked_read get a (i + 1) j)
    else
      Nil

let[@inline] unchecked_subarray a i j =
  unchecked_read Array.get a i j

let subarray a i j =
  let n = Array.length a in
  if not (0 <= i && i <= n) then
    invalid_arg "subarray: invalid start index";
  if not (i + j <= n) then
    invalid_arg "subarray: invalid end index";
  unchecked_subarray a i j

let array a =
  unchecked_subarray a 0 (Array.length a)

let[@inline] unchecked_substring s i j =
  unchecked_read String.get s i j

let substring s i j =
  let n = String.length s in
  if not (0 <= i && i <= n) then
    invalid_arg "substring: invalid start index";
  if not (i + j <= n) then
    invalid_arg "substring: invalid end index";
  unchecked_substring s i j

let string s =
  unchecked_substring s 0 (String.length s)

let iterator it =
  let rec c () =
    match it() with
    | None ->
        Nil
    | Some x ->
        Cons (x, c)
  in
  c

let channel c =
  iterator (fun () ->
    try
      Some (input_char c)
    with End_of_file ->
      close_in c;
      None
  )

(* If [f] maps one element [x] to a producer, then [fold f xs] produces
   the concatenation of the sequences [f x], where [x] ranges over [xs].
   This function is defined as a fold(-right) over [xs].

   val fold: ('a -> 'b producer) -> 'a seq -> 'b producer

   [fold] is related to [Seq.flat_map], but has a different type:
   here, [f x] is expected to return a producer, as opposed to a sequence.

 *)

(* The type of [fold] is more general (and low-level) than the type exposed in
   the interface file. The latter contains two occurrences of ['b producer],
   which is synonymous with ['b node suspension -> 'b node suspension],
   whereas internally we allow ['b suspension -> 'b suspension]. *)

let rec fold
  (f : 'a -> 'b suspension -> 'b suspension)
  (xs : 'a seq) (k : 'b suspension)
: 'b suspension =
  fun () ->
    match force xs with
    | Nil ->
        force k
    | Cons (x, xs) ->
        force (f x (fold f xs k))

let[@inline] foreach xs f =
  fold f xs

(* [>>=], a.k.a. [bind], is defined in terms of [foreach].

let[@inline] (>>=) (xs : 'a producer) (f : 'a -> 'b producer) : 'b producer =
  foreach (run xs) f

 *)

(* [map] can be defined in terms of [unfold]. *)

let _map_from_unfold (f : 'a -> 'b) (xs : 'a seq) : 'b seq =
  unfold (fun xs ->
    match force xs with
    | Nil ->
        None
    | Cons (x, xs) ->
        Some (f x, xs)
  ) xs

(* [map] can be defined in terms of [foreach]. *)

let _map_from_foreach (f : 'a -> 'b) (xs : 'a seq) : 'b seq =
  foreach xs (fun x -> cons (f x)) nil

(* A direct definition of [map]. *)

let rec map f xs =
  fun () ->
    match force xs with
    | Nil ->
        Nil
    | Cons (x, xs) ->
        Cons (f x, map f xs)

(* [scanl]. *)

let rec tail_scanl (f : 's -> 'a -> 's) (s : 's) (xs : 'a seq) : 's seq =
  fun () ->
    match force xs with
    | Nil ->
        Nil
    | Cons (x, xs) ->
        let s = f s x in
        Cons (s, tail_scanl f s xs)

let scanl (f : 's -> 'a -> 's) (s : 's) (xs : 'a seq) : 's seq =
  cons s (tail_scanl f s xs)

(* [init] can be defined in terms of [map] and [up]. *)

let _init_from_map_up n (f : int -> 'a) =
  map f (up 0 n)

(* A direct definition of [init]. *)

let rec init_aux (f : int -> 'a) i j =
  fun () ->
    if i < j then begin
      Cons (f i, init_aux f (i + 1) j)
    end
    else
      Nil

let[@inline] init n f =
  init_aux f 0 n

let[@inline] flatten (xss : 'a seq seq) : 'a seq =
  fold append xss nil

let rec filter f xs =
  fun () ->
    match force xs with
    | Nil ->
        Nil
    | Cons (x, xs) ->
        if f x then
          Cons (x, filter f xs)
        else
          force (filter f xs)

let rec while_ f xs =
  fun () ->
    match force xs with
    | Nil ->
        Nil
    | Cons (x, xs) ->
        if f x then
          Cons (x, while_ f xs)
        else
          Nil

let[@inline] until f xs =
  while_ (fun x -> not (f x)) xs

let rec on bs xs =
  fun () ->
    (* TODO avoid double force *)
    match force bs, force xs with
    | _, Nil
    | Nil, _ ->
        Nil
    | Cons (b, bs), Cons (x, xs) ->
        if b then
          Cons (x, on bs xs)
        else
          force (on bs xs)

(* A straightforward definition of [rev]. *)

let _rev xs =
  fun () -> force (
    fold_left (fun ys x -> cons x ys) nil xs
  )

(* This definition is [rev] is probably slightly more efficient. It uses
   ordinary list cells instead of a linked list of closures; this should
   save one word per cons cell. *)

let rev xs =
  fun () -> force (
    list (rev_to_list xs)
  )

let rec memoize xs =
  Suspension.memoize (fun () ->
    match force xs with
    | Nil ->
        Nil
    | Cons (x, xs) ->
        Cons (x, memoize xs)
  )

let rec once xs =
  Suspension.once (fun () ->
    match force xs with
    | Nil ->
        Nil
    | Cons (x, xs) ->
        Cons (x, once xs)
  )

let precompute xs =
  list (to_list xs)

let rec map2 f xs ys =
  fun () ->
    match force xs with
    | Nil ->
        Nil
    | Cons (x, xs) ->
        match force ys with
        | Nil ->
            Nil
        | Cons (y, ys) ->
            Cons (f x y, map2 f xs ys)

let zip xs ys =
  map2 (fun a b -> (a, b)) xs ys

(* An alternate definition of [on] in terms of [zip], [filter], [map]. *)

let _on bs xs =
  zip bs xs
  |> filter fst
  |> map snd

(* -------------------------------------------------------------------------- *)

(* Bit packing and unpacking. *)

(* A bit can be represented either as a Boolean or as an integer. *)

let b2i (b : bool) : int =
  if b then 1 else 0

let i2b (i : int) : bool =
  assert (i = 0 || i = 1);
  i = 1

(* [unpack_int k i] is a producer. It produces the [k] least significant bits of
   the integer [i]. It is not lazy; [k] memory cells are allocated immediately.
   The bits are written from left to right: the most significant bit comes
   first. *)

let rec unpack_int (k : int) (i : int) (bs : bool seq) : bool seq =
  if k = 0 then
    bs
  else
    (* Among the [k] rightmost bits of [i], compute the most significant one.
       This can be done by shifting [i] to the right by [k - 1] and reading
       the rightmost bit. *)
    let bit = (i lsr (k - 1)) land 1 in
    (* Continue. *)
    cons
      (i2b bit)
      (unpack_int (k - 1) i bs)

(* [unpack k xs] is a consumer of the [k]-bit integer sequence [xs] and a bit
   producer. *)

let unpack (k : int) (xs : int seq) : bool seq =
  assert (k > 0);
  fold (unpack_int k) xs nil

(* [pack_int k accu] is a consumer. It reads up to [k] bits from its argument
   and inserts them at the right end of the accumulator, which is shifted
   towards the left. If the end of the sequence is reached before [k] bits have
   been read, an appropriate number of zero bits are inserted at the end as
   padding. The function returns the updated accumulator and the remainder of
   the sequence. *)

let rec pack_int (k : int) (accu : int) (bs : bool seq) : int * bool seq =
  if k = 0 then
    accu, bs
  else
    let b, bs =
      match force bs with
      | Nil ->
          0, bs (* padding *)
      | Cons (b, bs) ->
          b2i b, bs
    in
    pack_int (k - 1) (accu lsl 1 lor b) bs

(* [pack k bs] is a consumer of the bit sequence [bs] and a [k]-bit integer
   producer. If the number of elements in the sequence [bs] is not a multiple
   of [k], an appropriate number of zero bits are inserted at the end as
   padding. *)

let pack (k : int) (bs : bool seq) : int seq =
  assert (k > 0);
  unfold (fun bs ->
    match force bs with
    | Nil ->
        (* If the bit sequence is finished, we are finished too. *)
        None
    | Cons (b, bs) ->
        (* The bit sequence is not finished, so we must produce one integer by
           reading (up to) [k] bits. *)
        let i, bs = pack_int (k - 1) (b2i b) bs in
        Some (i, bs)
  ) bs

end (* P *)

(* -------------------------------------------------------------------------- *)

(* The [bind] combinator of the sequence monad. *)

(* [bind] in the sequence monad is not efficient, as it uses [append]. *)

let (>>=) (xs : 'a seq) (f : 'a -> 'b seq) : 'b seq =
  P.fold (fun x ys ->
    append (f x) ys
  ) xs nil

(* -------------------------------------------------------------------------- *)

(* Infinite sequences. *)

(* A simple definition of [repeat]. *)

let rec _repeat (x : 'a) : 'a seq =
  fun () ->
    Cons (x, _repeat x)

(* This definition should be more efficient, as it constructs
   a closure and a [Cons] cell that point to each other, and
   thereby avoids allocating a new closure and a new cell
   every time an element is demanded. *)

let repeat x =
  let rec r () = c
  and c = Cons (x, r) in
  r

let rec unfold_infinitely (f: 's -> 'a * 's) (s : 's) =
  fun () ->
    let x, s = f s in
    Cons (x, unfold_infinitely f s)

let rec ints i : int seq =
  fun () ->
    Cons (i, ints (i + 1))

let rec iterate1 (f : 'a -> 'a) (x : 'a) : 'a seq =
  fun () ->
    let y = f x in
    Cons (y, iterate1 f y)

let iterate f x =
  cons x (iterate1 f x)

let cycle xs =
  let rec c () =
    force (xs c) (* let [xs] eat itself! *)
  in
  c

let fix =
  Suspension.fix (* this is a little magic! *)

(* -------------------------------------------------------------------------- *)

(* Joining. *)

(* One might wonder whether [merge] and [interleave] should be producer
   combinators, instead of sequence combinators. That would involve
   parameterizing them with a continuation [k], threading this continuation
   throughout, and, when one sequence (say, [xs]) vanishes, returning the
   concatenation of the other sequence (say, [ys]) with [k]. That would be
   marginally more efficient than naively using [append] outside of [merge] or
   [interleave]. However, it would not be entirely free: indeed, threading [k]
   throughout requires reserving one more word of memory in every [Cons] cell.
   For now, this does not seem to be worth the trouble. *)

let rec merge1l cmp x xs ys =
  fun () ->
    match force ys with
    | Nil ->
        Cons (x, xs)
    | Cons (y, ys) ->
        merge1 cmp x xs y ys

and merge1r cmp xs y ys =
  fun () ->
    match force xs with
    | Nil ->
        Cons (y, ys)
    | Cons (x, xs) ->
        merge1 cmp x xs y ys

and merge1 cmp x xs y ys =
  if cmp x y <= 0 then
    Cons (x, merge1r cmp xs y ys)
  else
    Cons (y, merge1l cmp x xs ys)

let merge cmp xs ys =
  fun () ->
    match force xs, force ys with
    | Nil, Nil ->
        Nil
    | Nil, c
    | c, Nil ->
        c
    | Cons (x, xs), Cons (y, ys) ->
        merge1 cmp x xs y ys

let rec interleave xs ys =
  fun () ->
    match force xs with
    | Nil ->
        force ys
    | Cons (x, xs) ->
        Cons (x, interleave ys xs)

(* -------------------------------------------------------------------------- *)

(* Forking. *)

(* Although the following functions could return producers, as opposed to
   sequences, this seems needlessly complex. *)

let unzip xys =
  map fst xys,
  map snd xys

type ('a, 'b) either =
| Left of 'a
| Right of 'b

let is_left (x : ('a, 'b) either) : 'a seq -> 'a seq =
  match x with
  | Left a ->
      cons a
  | Right _ ->
      fun k -> k

let is_right (x : ('a, 'b) either) : 'b seq -> 'b seq =
  match x with
  | Left _ ->
      fun k -> k
  | Right b ->
      cons b

let dispatch (xs : ('a, 'b) either seq) : 'a seq * 'b seq =
  P.fold is_left xs nil,
  P.fold is_right xs nil
  (* TODO rewrite using [filter_map] *)

(* We have several possible definitions of [partition], as follows.
   We choose the most friendly, which ensures that [xs] is forced
   at most once and [f] is applied at most once to each element [x]. *)

let _partition (f : 'a -> bool) (xs : 'a seq) : 'a seq * 'a seq =
  dispatch (map (fun x -> if f x then Left x else Right x) xs)
  (* bad: [f] can be called twice per element, even if [xs] is memoized *)

let _partition (f : 'a -> bool) (xs : 'a seq) : 'a seq * 'a seq =
  filter f xs,
  filter (fun x -> not (f x)) xs
  (* bad: [f] can be called twice per element, even if [xs] is memoized *)

let partition (f : 'a -> bool) (xs : 'a seq) : 'a seq * 'a seq =
  dispatch (P.memoize (map (fun x -> if f x then Left x else Right x) xs))
  (* good: [f] is called at most once per element *)

let rec uniq1 eq x (ys : 'a seq) : 'a seq =
  fun () ->
    match force ys with
    | Nil ->
        Nil
    | Cons (y, ys) ->
        if eq x y then
          force (uniq1 eq x ys)
        else
          Cons (y, uniq1 eq y ys)

let uniq eq (xs : 'a seq) : 'a seq =
  fun () ->
    match force xs with
    | Nil ->
        Nil
    | Cons (x, ys) ->
        Cons (x, uniq1 eq x ys)

(* -------------------------------------------------------------------------- *)

(* Merge sort. For fun. Adapted from Haskell's Prelude. *)

(* [merge_pairs xss] requires [xss] to be a sequence of sorted sequences. It
   merges every two consecutive elements, yielding a new sequence (of sorted
   sequences) whose length is [(n+1)/2] where [n] is the length of [xss]. *)

let rec merge_pairs cmp (xss : 'a seq seq) : 'a seq seq =
  (* This was an instance of [unfold], but has been specialized. *)
  fun () ->
    match force xss with
    | Nil ->
        Nil
    | Cons (xs, xss) ->
        match force xss with
        | Nil ->
            Cons (xs, nil)
        | Cons (ys, xss) ->
            Cons (merge cmp xs ys, merge_pairs cmp xss)

let rec force_mergesort_cons cmp xs (xss : 'a seq seq) : 'a node =
  match force xss with
  | Nil ->
      force xs
  | Cons (ys, xss) ->
      force_mergesort_cons cmp (merge cmp xs ys) (merge_pairs cmp xss)

let force_mergesort cmp (xss : 'a seq seq) : 'a node =
  match force xss with
  | Nil ->
      Nil
  | Cons (xs, xss) ->
      force_mergesort_cons cmp xs xss

let sort cmp (xs : 'a seq) : 'a seq =
  fun () ->
    force_mergesort cmp (map singleton xs)
