type 'a suspension =
  unit -> 'a

(* -------------------------------------------------------------------------- *)

(* Conversions between the types [Lazy.t] and [suspension]. *)

let from_lazy (s : 'a Lazy.t) : 'a suspension =
  fun () -> Lazy.force s

let to_lazy : 'a suspension -> 'a Lazy.t =
  Lazy.from_fun
  (* fun s -> lazy (force s) *)

(* -------------------------------------------------------------------------- *)

(* Memoization. *)

let memoize (f : 'a suspension) : 'a suspension =
  from_lazy (to_lazy f)

(* -------------------------------------------------------------------------- *)

(* Fixed points. *)

(* A fixed point combinator the type [Lazy.t]. *)

let fix (f : 'a Lazy.t -> 'a Lazy.t) : 'a Lazy.t =
  (* Thanks to OCaml's [let rec] construct, there is no need to backpatch
     a reference. *)
  let rec s = lazy (
    Lazy.force (f s)
  ) in
  s

(* We could wrap the call to [Lazy.force] above in a handler for the exception
   [Lazy.Undefined]. This would catch the case where the function [f] is not
   productive, and is not even able to produce the head of the sequence.
   However, this would not catch the cases where the head of the sequence can
   be produced, yet a productivity failure occurs later on. The recursive
   equation [xs = 0 :: tail xs] is an example. It does not fail when [xs] is
   constructed or when its head is demanded; it fails when its tail is
   demanded. In order to also catch this mistake, we would have to also wrap
   the call to [Lazy.force] in [from_lazy]. This does not seem to be worth the
   cost, especially in light of the fact that the benefit is unclear: tracing
   the origin of the exception [Lazy.Undefined] is not more difficult than
   tracing the origin of the exception that a wrapper would raise. *)

(* A fixed point combinator for the type [suspension]. *)

(* The wrapping and unwrapping around [f] has little cost, since [f] is
   invoked at most once. *)

let fix (f : 'a suspension -> 'a suspension) : 'a suspension =
  from_lazy (fix (fun s ->
    to_lazy (f (from_lazy s))
  ))

(* -------------------------------------------------------------------------- *)

(* Detecting violations of affinity. *)

exception ForcedTwice

let failure =
  fun () ->
    (* A suspension created by [once] has been forced twice. *)
    raise ForcedTwice

(* If [f] is a suspension, then [once f] is a suspension that can be called
   at most once. If it is invoked more than once, then the exception
   [ForcedTwice] is raised. *)

let once (f : 'a suspension) : 'a suspension =
  let action = ref f in
  fun () ->
    let f = !action in
    action := failure;
    f()
    (* We are careful to avoid closing over [f], as that might cause a
       memory leak: we would retain a pointer to [f] even after the
       suspension has been forced. That said, an ordinary suspension of
       type [unit -> 'a] has this memory leak already. (A suspension of
       type ['a Lazy.t] does not have this problem.) *)
