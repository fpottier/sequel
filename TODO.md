# To do

* Finish getting rid of producers. Decide how to name the producer functions:
  systematically use an `of_` prefix? Keep the submodule `P`?

* What about Cartesian product, fair Cartesian product, etc.?

* Better understand the difference between `cycle` and `fix`.
  Remove them?

* Add a variant of `flatten` that consume a list, for syntactic convenience.

* Explain that concatenation of sequences is tolerable as long as it is not
  nested on the left.

* Check that every conversion of something to a sequence has a matching
  conversion in the reverse direction.

* Add `iteri`, `mapi`.

* Add `map2`, `fold_left2`, `for_all2`, `exists2`. With what semantics when
  the lengths of the two sequences differ? Answer: require the lengths to
  match. If the user wants to handle the case where they differ, she should
  use iterators.

* Add pretty-printing functions.

* Add a `k`-way merge. (Requires a priority queue.)

* Explain `fringe` in the README.

* Test the functional correctness of every operation. So far, *no serious
  testing* has been performed.

* Test the laziness of every operation. So far, *no serious testing* has been
  performed.

* Ideally, the description of each combinator should describe whether the
  sequence that is constructed is ephemeral or persistent. When it takes a
  sequence as an argument, it should also describe when and how many times
  this sequence is forced. Offering an accurate yet understandable description
  is difficult.
